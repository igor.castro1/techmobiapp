import "reflect-metadata";
import "react-native-gesture-handler";
import React from "react";
import store from "./view/store/store";
import Router from "./view/router/Router";
import { Provider } from "react-redux";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { QueryClientProvider, QueryClient } from "react-query";

const queryClient = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <GestureHandlerRootView
          style={{
            flex: 1,
          }}
        >
          <Router />
        </GestureHandlerRootView>
      </Provider>
    </QueryClientProvider>
  );
}
