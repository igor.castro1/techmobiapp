import axios, { AxiosInstance } from "axios";

type HeadersType = {
  [key: string]: string;
};

class AxiosDomain {
  private baseUrl: string;
  private headers: HeadersType;
  private withCredentials: boolean;

  constructor(baseUrl: string, headers: HeadersType, withCredentials: boolean) {
    this.baseUrl = baseUrl;
    this.headers = headers;
    this.withCredentials = withCredentials;
  }

  initInstance(): AxiosInstance {
    return axios.create({
      baseURL: this.baseUrl,
      headers: this.headers,
      withCredentials: this.withCredentials,
    });
  }
}

export class AxiosBuilder {
  public baseUrl = "";
  public headers: HeadersType = {
    "Content-Type": "application/json",
  };
  public withCredentials = false;

  static create(): AxiosBuilder {
    return new AxiosBuilder();
  }

  toDomain(): AxiosDomain {
    return new AxiosDomain(this.baseUrl, this.headers, this.withCredentials);
  }

  withBaseUrl(url: string) {
    this.baseUrl = url;
    return this;
  }

  withHeaders(headerConfiguration: HeadersType) {
    this.headers = headerConfiguration;
    return this;
  }

  haveCredentials() {
    this.withCredentials = true;
    return this;
  }
}
