export enum ETicketStatus {
  Overweight = 1,
  Created,
  Available,
  Deleted,
}
