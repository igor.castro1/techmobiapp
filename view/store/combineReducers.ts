import { combineReducers } from "@reduxjs/toolkit";
import * as REDUCERS from "./reducers/imports";

export const rootReducer = combineReducers({
  register: REDUCERS.registerReducer,
});
