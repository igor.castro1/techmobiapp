import {
  useFonts,
  DMSans_400Regular,
  DMSans_500Medium,
  DMSans_700Bold,
} from "@expo-google-fonts/dm-sans";

const UseFontLoaded = () => {
  const [fontLoaded] = useFonts({
    DMSans_400Regular,
    DMSans_500Medium,
    DMSans_700Bold,
  });

  return {
    fontLoaded,
  };
};

export default UseFontLoaded;

export const StaticFonts = {
  400: "DMSans_400Regular",
  500: "DMSans_500Medium",
  700: "DMSans_700Bold",
};
