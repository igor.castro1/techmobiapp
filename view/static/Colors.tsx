export const Colors = {
  main: "#f38021",
  mainContrast:  "#ffffff",
  info: "#808080",
  black: "#2f2f2f",
  inputBorder: "rgb(222,222,222)",
  transparent: "#feb67a",
};
