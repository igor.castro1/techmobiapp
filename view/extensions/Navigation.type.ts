import { NavigationProp } from "@react-navigation/native";

type RootStackParamList = {
  [key: string]: any;
};

export type CommonNavigationProps = {
  navigation: NavigationProp<RootStackParamList>;
};

