import { View } from "react-native";
import React from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import UseBaseFooterViewController from "./BaseFooterVIew.controller";
import { styles } from "./BaseFooterView.styles";
import {
  TicketIcon,
  HomeIcon,
  SearchIcon,
  NotificationIcon,
  UserDefaultIcon,
} from "../Icons/Icons";
import { TouchableOpacity } from "react-native-gesture-handler";

interface Props extends CommonNavigationProps {
  children: JSX.Element;
}

const BaseFooterView = ({ navigation, children }: Props) => {
  const { Actions, States } = UseBaseFooterViewController({ navigation });

  return (
    <View style={styles.container}>
      {children}
      <View style={styles.iconsContainer}>
        <TouchableOpacity onPress={Actions.onNavigateTickets}>
          <TicketIcon size={States.iconSize} color={States.mainIconColor} />
        </TouchableOpacity>
        <TouchableOpacity onPress={Actions.onNavigateSearch}>
          <SearchIcon size={States.iconSize} color={States.mainIconColor} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.homeIcon} onPress={Actions.onNavigateHome}>
          <HomeIcon size={States.iconSize} color={States.homeIconColor} />
        </TouchableOpacity>
        <TouchableOpacity onPress={Actions.onNavigateNotifications}
        >
          <NotificationIcon
            size={States.iconSize}
            color={States.mainIconColor}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={Actions.onNavigateUser}>
          <UserDefaultIcon
            size={States.iconSize}
            color={States.mainIconColor}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BaseFooterView;
