import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { Colors } from "../../static/Colors";

export const styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    backgroundColor: Colors.main,
  },
  iconsContainer: {
    width: deviceWidth,
    height: deviceHeight / 11,
    borderRadius: 25,
    backgroundColor: "white",
    position: "absolute",
    bottom: 0,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: deviceWidth / 14,
  },
  homeIcon: {
    width: deviceWidth / 5,
    height: deviceHeight / 10.5,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 2,
    position: "relative",
    top: -35,
    backgroundColor: Colors.main,
    borderRadius: 25,
    shadowColor: "black",
  },
});
