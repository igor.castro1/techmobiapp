import { useCallback, useMemo } from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import { Colors } from "../../static/Colors";
import { deviceWidth } from "../../static/Dimensions";

const UseBaseFooterViewController = ({ navigation }: CommonNavigationProps) => {
  const defaultIconSize = useMemo(() => {
    return deviceWidth / 14;
  }, []);

  const mainIconColor = useMemo(() => {
    return Colors.main;
  }, []);

  const homeIconColor = useMemo(() => {
    return Colors.mainContrast;
  }, []);

  const navigateTickets = useCallback(() => {
    navigation.navigate("Tickets");
  }, []);
  const navigateSearch = useCallback(() => {
    navigation.navigate("Search");
  }, []);
  const navigateHome = useCallback(() => {
    navigation.navigate("Home");
  }, []);
  const navigateNotifications = useCallback(() => {
    navigation.navigate("Notifications");
  }, []);
  const navigateUser = useCallback(() => {
    navigation.navigate("User");
  }, []);

  return {
    Actions: {
      onNavigateTickets: navigateTickets,
      onNavigateSearch: navigateSearch,
      onNavigateHome: navigateHome,
      onNavigateNotifications: navigateNotifications,
      onNavigateUser: navigateUser,
    },
    States: {
      iconSize: defaultIconSize,
      mainIconColor,
      homeIconColor,
    },
  };
};

export default UseBaseFooterViewController;
