import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../../static/Dimensions";
import { Colors } from "../../../static/Colors";
import { StaticFonts } from "../../../static/Fonts";

export const textInputStyle = StyleSheet.create({
  component: {
    width: "100%",
    position: "relative",
  },
  iconContainer: {
    position: "absolute",
    left: 5,
    top: 5,
    width: deviceWidth / 8,
    height: deviceHeight / 17,
    backgroundColor: Colors.main,
    borderRadius: 30,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1,
  },
  input: {
    backgroundColor: Colors.mainContrast,
    height: deviceHeight / 14.5,
    width: "100%",
    borderRadius: 40,
    borderWidth: 1,
    borderColor: Colors.inputBorder,
    paddingLeft: 65,
    paddingRight: 20,
    fontSize: 16,
    fontWeight: "300",
    fontFamily: StaticFonts[400],
  },
});
