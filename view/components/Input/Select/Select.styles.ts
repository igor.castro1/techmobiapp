import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  inputIOS: {
    fontSize: 13,
    color: "white",
  },
  inputAndroid: {
    fontSize: 13,
    color: "white",
  },
});
export const containerStyle = StyleSheet.create({
  container: {
    width: "100%",
  },
});
