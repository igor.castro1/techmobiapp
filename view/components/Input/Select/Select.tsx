import { Text, View } from "react-native";
import React from "react";
import RNPickerSelect from "react-native-picker-select";
import { Controller } from "react-hook-form";
import { styles, containerStyle } from "./Select.styles";
import { Octicons } from "@expo/vector-icons";

type SelectProps = {
  items: Array<{ label: string; value: unknown }>;
  control: any;
  name: string;
  errors?: any;
};

const Select = ({ items, control, name, errors, ...other }: SelectProps) => {
  return (
    <View style={containerStyle.container}>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <RNPickerSelect
            useNativeAndroidPickerStyle={false}
            onValueChange={onChange}
            Icon={() => (
              <Octicons name="triangle-down" size={16} color="white" />
            )}
            items={items}
            value={value}
            style={styles}
            placeholder={{ label: "", value: null }}
            {...other}
          />
        )}
        name={name}
      />
      {errors ? <>{errors[name] && <Text>{errors[name].message}</Text>}</> : ""}
    </View>
  );
};

export default Select;
