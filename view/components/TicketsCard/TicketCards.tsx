import Container from "./Container/Container";
import TicketImage from "./Image/Image";
import Content from "./Content/Content";

export const TicketCard = {
  Container,
  TicketImage,
  Content,
};
