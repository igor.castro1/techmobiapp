import { View, Image, TouchableOpacity } from "react-native";
import BorderStyle from "../../../../assets/border_style.png";
import React from "react";
import UseContainerController from "./Container.controller";
import { styles } from "./Container.styles";

type ContainerProps = {
  children: React.ReactNode;
  haveBorder?: boolean;
  onPress: () => void;
};

const Container = ({ children, haveBorder, onPress }: ContainerProps) => {
  const { States } = UseContainerController({
    haveBorder: haveBorder ?? false,
  });
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.touchableOpacityContainer}
    >
      <View style={States.currentContainerStyle}>
        {children}
        {!haveBorder && <View style={styles.invisibleView}></View>}
        {haveBorder && (
          <Image source={BorderStyle} style={styles.borderImageStyle} />
        )}
      </View>
    </TouchableOpacity>
  );
};

export default Container;
