import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../../static/Dimensions";

export const styles = StyleSheet.create({
  touchableOpacityContainer: {
    width: "100%",
    height: deviceHeight / 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  containerWithoutBorder: {
    backgroundColor: "white",
    maxWidth: "90%",
    width: "100%",
    height: deviceHeight / 8,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderTopLeftRadius: deviceWidth / 60,
    borderBottomLeftRadius: deviceWidth / 60,
    paddingLeft: 10,
    shadowColor: "#171717",
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  containerWithBorder: {
    backgroundColor: "white",
    maxWidth: "90%",
    width: "100%",
    height: deviceHeight / 8,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderTopLeftRadius: deviceWidth / 60,
    borderBottomLeftRadius: deviceWidth / 60,
    paddingLeft: 10,
    shadowColor: "#17171786",
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  borderImageStyle: {
    height: "100%",
    width: "4%",
    objectFit: "cover",
    position: "relative",
    // left: 5,
  },
  invisibleView: {
    width: "4%",
  },
});
