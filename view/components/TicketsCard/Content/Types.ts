import { EVehicleType } from "../../../../business/domain/enums/EVehicleType";

type TVehicle = {
  type: EVehicleType;
  vehiclePlate: string;
};

type TDates = {
  createdAt: Date;
  timeToNow: boolean;
  invert: boolean;
};

export type ContentProps = {
  ticketNumber: number | string;
  ticketStatus: string;
  vehicle?: TVehicle;
  dates: TDates;
};
