import { Text, View } from "react-native";
import React from "react";
import { styles } from "./Content.styles";
import UseContentController from "./Content.controller";
import { ContentProps } from "./Types";

const Content = ({
  ticketNumber,
  ticketStatus,
  vehicle,
  dates,
}: ContentProps) => {
  const { Actions, Helpers } = UseContentController({
    ticketNumber,
    ticketStatus,
    vehicle,
    dates,
  });

  return (
    <View style={styles.container}>
      <View style={styles.ticketContainer}>
        <Text style={styles.ticketText}>{ticketNumber}</Text>
        <Text style={styles.timeText}>
          {!dates.invert
            ? Actions.checkIfCreatedDateExist()
            : Actions.checkIfDateToNowExist()}
        </Text>
      </View>
      <View style={styles.statusContainer}>
        <Text style={styles.statusText}>{ticketStatus}</Text>
      </View>
      <View style={styles.vehicleInformationContainer}>
        <Text style={styles.truckInformationText}>
          {Actions.onConvertVehicle()}
        </Text>
        <Text style={styles.truckInformationText}>{vehicle?.vehiclePlate}</Text>
        <Text style={styles.invertedDateText}>
          {dates.invert
            ? Actions.checkIfCreatedDateExist()
            : Actions.checkIfDateToNowExist()}
        </Text>
      </View>
    </View>
  );
};

export default Content;
