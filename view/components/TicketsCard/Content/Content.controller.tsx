import React, { useCallback } from "react";
import { ContentProps } from "./Types";
import { Text } from "react-native";
import { DateFacade } from "../../../../business/service/date.facade";

const UseContentController = ({ vehicle, dates }: ContentProps) => {
  const dateHelper = new DateFacade();

  const checkIfCreatedDateExist = useCallback(() => {
    if (dates.createdAt) return dateHelper.format(dates.createdAt);
    return "";
  }, []);

  const checkIfDateToNowExist = useCallback(() => {
    if (dates.timeToNow && dates.createdAt) return "5h ago";
    return "";
  }, []);

  const convertEnumeratorVehicleIntoCorrespondingString = useCallback(() => {
    if (vehicle?.type) {
      return vehicle.type === 1 ? "Truck" : "Car";
    }
    return "";
  }, []);

  return {
    Actions: {
      onConvertVehicle: convertEnumeratorVehicleIntoCorrespondingString,
      checkIfCreatedDateExist,
      checkIfDateToNowExist,
    },
    Helpers: {
      date: dateHelper,
    },
  };
};

export default UseContentController;
