import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  image: {
    height: "70%",
    width: "20%",
    objectFit: "contain",
    borderRadius: 50,
  },
});
