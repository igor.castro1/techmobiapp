import React from "react";
import Svg, { Path } from "react-native-svg";
import { Colors } from "../../static/Colors";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";

const Wave = () => {
  return (
    <Svg height={deviceHeight} width={deviceWidth}>
      <Path
        fill={Colors.main}
        d="M 0 208 C 27.599999999999998 208 18.400000000000002 173 46 173 L 46 173 L 46 0 L 0 0 Z"
        strokeWidth="0"
      />
      <Path
        fill={Colors.main}
        d="M 45 173 C 75 173 65 139 95 139 L 95 139 L 95 0 L 45 0 Z"
        strokeWidth="0"
      />
      <Path
        fill={Colors.main}
        d="M 94 139 C 128.8 139 117.2 217 152 217 L 152 217 L 152 0 L 94 0 Z"
        strokeWidth="0"
      />
      <Path
        fill={Colors.main}
        d="M 151 217 C 192.4 217 178.6 185 220 185 L 220 185 L 220 0 L 151 0 Z"
        strokeWidth="0"
      />
      <Path
        fill={Colors.main}
        d="M 219 185 C 267 185 251 229 299 229 L 299 229 L 299 0 L 219 0 Z"
        strokeWidth="0"
      />
      <Path
        fill={Colors.main}
        d="M 298 229 C 361 229 340 169 403 169 L 403 169 L 403 0 L 298 0 Z"
        strokeWidth="0"
      />
      <Path
        fill={Colors.main}
        d="M 402 169 C 433.8 169 423.2 205 455 205 L 455 205 L 455 0 L 402 0 Z"
        strokeWidth="0"
      />
      <Path
        fill="rgba(47, 73, 94, 1)"
        d="M 454 205 C 1271.8 205 999.2 50 1817 50 L 1817 50 L 1817 0 L 454 0 Z"
        strokeWidth="0"
      />
    </Svg>
  );
};

export default Wave;
