import { StyleSheet } from "react-native";
import { Colors } from "../../static/Colors";
import { StaticFonts } from "../../static/Fonts";

export const styles = StyleSheet.create({
  buttonDefault: {
    width: "100%",
    height: 60,
    backgroundColor: Colors.main,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
  },
  buttonWhite: {
    width: "100%",
    height: 60,
    backgroundColor: Colors.transparent,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
  },
  text: {
    color: Colors.mainContrast,
    fontSize: 16,
    fontFamily: StaticFonts[700],
  },
});
