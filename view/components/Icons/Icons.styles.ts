import { StyleSheet } from "react-native";
import { Colors } from "../../static/Colors";

export const styles = StyleSheet.create({
  icon: { color: Colors.mainContrast },
});
