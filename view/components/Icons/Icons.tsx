import React from "react";
import { defaultIconProps } from "../../static/Icon";
import { AntDesign } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { styles } from "./Icons.styles";
import { Feather } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { Foundation } from "@expo/vector-icons"; 
import { MaterialIcons } from "@expo/vector-icons";
import { SimpleLineIcons } from "@expo/vector-icons";

export const UserIcon = () => {
  return (
    <AntDesign
      style={styles.icon}
      name="user"
      size={defaultIconProps.size}
    />
  );
};
export const EmailIcon = () => {
  return (
    <MaterialCommunityIcons
      style={styles.icon}
      name="email-outline"
      size={defaultIconProps.size}
    />
  );
};
export const LockIcon = () => {
  return (
    <Feather
      style={styles.icon}
      name="lock"
      size={defaultIconProps.size}
    />
  );
};

type DefaultProps = {
  size: number;
  color: string;
};

export const TicketIcon = ({ size, color, ...other }: DefaultProps) => {
  return (
    <MaterialCommunityIcons
      name="ticket-confirmation-outline"
      {...other}
      size={size}
      color={color}
    />
  );
};
export const HomeIcon = ({ size, color, ...other }: DefaultProps) => {
  return <AntDesign name="home" {...other} size={size} color={color} />;
};
export const SearchIcon = ({ size, color, ...other }: DefaultProps) => {
  return <AntDesign name="search1" {...other} size={size} color={color} />;
};
export const NotificationIcon = ({ size, color, ...other }: DefaultProps) => {
  return (
    <Ionicons
      name="notifications-outline"
      {...other}
      size={size}
      color={color}
    />
  );
};
export const UserDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <Feather name="user" {...other} size={size} color={color} />;
};
export const FilterDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <Ionicons name="filter" {...other} size={size} color={color} />;
};
export const DashboardDefaultIcon = ({
  size,
  color,
  ...other
}: DefaultProps) => {
  return (
    <MaterialCommunityIcons
      name="view-dashboard-outline"
      {...other}
      size={size}
      color={color}
    />
  );
};

export const PlusDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <Foundation name="plus" {...other} size={size} color={color} />;
};
export const LockDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <MaterialIcons name="lock-outline" {...other} size={size} color={color} />;
};
export const EarPhonesDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <SimpleLineIcons name="earphones-alt" {...other} size={size} color={color} />;
};
export const InfoDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <AntDesign name="infocirlceo" {...other} size={size} color={color} />;
};
export const LanguageDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <Ionicons name="language" {...other} size={size} color={color} />;
};
export const LogoutDefaultIcon = ({ size, color, ...other }: DefaultProps) => {
  return <MaterialIcons name="logout" {...other} size={size} color={color} />;
};
