import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { Stack } from "./Stack";
import { RouterProperties as ROUTES } from "./Router.properties";
import UseFontLoaded from "../static/Fonts";

const Router = () => {
  const defaultHeaderOptionsConfiguration = {
    headerShown: false,
  };
  const { fontLoaded } = UseFontLoaded();

  if (!fontLoaded) {
    return null;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={ROUTES.welcome.name}>
        <Stack.Screen
          name={ROUTES.login.name}
          component={ROUTES.login.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.register.name}
          component={ROUTES.register.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.welcome.name}
          component={ROUTES.welcome.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.tickets.name}
          component={ROUTES.tickets.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.search.name}
          component={ROUTES.search.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.home.name}
          component={ROUTES.home.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.notification.name}
          component={ROUTES.notification.component}
          options={defaultHeaderOptionsConfiguration}
        />
        <Stack.Screen
          name={ROUTES.user.name}
          component={ROUTES.user.component}
          options={defaultHeaderOptionsConfiguration}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
