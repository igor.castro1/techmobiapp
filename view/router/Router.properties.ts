import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login";
import Notification from "../pages/Notification/Notification";
import Register from "../pages/Register/Register";
import Search from "../pages/Search/Search";
import Tickets from "../pages/Tickets/Tickets";
import User from "../pages/User/User";
import Welcome from "../pages/Welcome/Welcome";

export const RouterProperties = {
  login: {
    name: "Login",
    component: Login,
  },
  register: {
    name: "Register",
    component: Register,
  },
  welcome: {
    name: "Welcome",
    component: Welcome,
  },
  home: {
    name: "Home",
    component: Home,
  },
  tickets: {
    name: "Tickets",
    component: Tickets,
  },
  search: {
    name: "Search",
    component: Search,
  },
  notification: {
    name: "Notifications",
    component: Notification,
  },
  user: {
    name: "User",
    component: User,
  },
};
