import * as yup from "yup";

export const RegisterFormSchema = yup.object({
  user: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().required(),
});
