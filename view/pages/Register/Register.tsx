import { View, Text, Image } from "react-native";
import WhiteLogo from "../../../assets/techmobi_white_logo.png";
import React from "react";
import UseRegisterController from "./Register.controller";
import { styles } from "./Register.styles";
import Wave from "../../components/Wave/Wave";
import CustomText from "../../components/Input/CustomTextInput/CustomText";
import { EmailIcon, LockIcon, UserIcon } from "../../components/Icons/Icons";
import CustomButton from "../../components/CustomButton/CustomButton";
import { TouchableOpacity } from "react-native-gesture-handler";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

const Register = ({ navigation }: CommonNavigationProps) => {
  const { Actions, Helpers } = UseRegisterController({ navigation });
  return (
    <View style={styles.component}>
      <View style={styles.waveView}>
        <Image source={WhiteLogo} style={styles.logo} />
        <Wave />
      </View>
      <View style={styles.textView}>
        <View style={styles.form}>
          <Text style={styles.formTitle}>Create an Account</Text>
          <Text style={styles.formExplanationText}>
            Loren ipsum dolor ist amet, consectetur adipiscing elit, sed do
            eisumd
          </Text>
          <CustomText
            name="user"
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<UserIcon />}
            placeholder="Name"
          />
          <CustomText
            name="email"
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<EmailIcon />}
            placeholder="Enter your E-mail Address"
          />
          <CustomText
            name="password"
            textContentType="password"
            secureTextEntry
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<LockIcon />}
            placeholder="Password"
          />
          <CustomButton title="NEXT" onPress={() => console.log("pressed")} />
          <View style={styles.newAccountSectionComponent}>
            <Text style={styles.newAccountExplanationText}>
              Already have an account?
            </Text>
            <TouchableOpacity onPress={Actions.onNavigateLogin}>
              <Text style={styles.newAccountNavigatorText}>Sign in here</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Register;
