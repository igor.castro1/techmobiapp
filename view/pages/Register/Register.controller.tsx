import React, { useCallback } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { RegisterFormSchema } from "./Register.validation";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

type RegisterFormType = {
  user: string;
  email: string;
  password: string;
};

const UseRegisterController = ({ navigation }: CommonNavigationProps) => {
  const {
    handleSubmit,
    control,
    formState: { errors },
    watch,
  } = useForm<RegisterFormType>({
    resolver: yupResolver(RegisterFormSchema),
  });

  const navigateToLogin = useCallback(() => {
    navigation.navigate("Login");
  }, []);

  const onSubmit: SubmitHandler<RegisterFormType> = useCallback((data) => {
    console.log(data);
  }, []);

  return {
    Actions: {
      onSubmit: handleSubmit(onSubmit),
      onNavigateLogin: navigateToLogin,
    },
    Helpers: {
      errors,
      control,
    },
  };
};

export default UseRegisterController;
