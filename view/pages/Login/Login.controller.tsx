import { useCallback } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import { LoginFormSchema } from "./Login.validation";

type LoginFormType = {
  user: string;
  password: string;
};

const UseLoginController = ({ navigation }: CommonNavigationProps) => {
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<LoginFormType>({
    resolver: yupResolver(LoginFormSchema),
  });

  const navigateRegister = useCallback(() => {
    navigation.navigate("Register");
  }, []);

  const navigateHome = useCallback(() => {
    navigation.navigate("Home");
  }, []);

  const onSubmit: SubmitHandler<LoginFormType> = useCallback((data) => {
    console.log(data);
  }, []);

  return {
    Actions: {
      onSubmit: handleSubmit(onSubmit),
      onNavigateToRegister: navigateRegister,
      onNavigateHome: navigateHome,
    },
    Helpers: {
      errors,
      control,
    },
  };
};

export default UseLoginController;
