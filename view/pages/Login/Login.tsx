import { View, Text, Image } from "react-native";
import WhiteLogo from "../../../assets/techmobi_white_logo.png";
import React from "react";
import UseLoginController from "./Login.controller";
import { styles } from "./Login.styles";
import Wave from "../../components/Wave/Wave";
import CustomText from "../../components/Input/CustomTextInput/CustomText";
import { LockIcon, UserIcon } from "../../components/Icons/Icons";
import CustomButton from "../../components/CustomButton/CustomButton";
import { TouchableOpacity } from "react-native-gesture-handler";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

const Login = ({ navigation }: CommonNavigationProps) => {
  const { Actions, Helpers } = UseLoginController({ navigation });
  return (
    <View style={styles.component}>
      <View style={styles.waveView}>
        <Image source={WhiteLogo} style={styles.logo} />
        <Wave />
      </View>
      <View style={styles.textView}>
        <View style={styles.form}>
          <Text style={styles.formTitle}>Sign in</Text>
          <Text style={styles.formExplanationText}>
            Loren ipsum dolor ist amet, consectetur adipiscing elit, sed do
            eisumd
          </Text>
          <CustomText
            name="user"
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<UserIcon />}
            placeholder="Name"
          />
          <CustomText
            name="password"
            textContentType="password"
            secureTextEntry
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<LockIcon />}
            placeholder="Password"
          />
          <CustomButton title="SIGN IN" onPress={Actions.onNavigateHome} />
          <View style={styles.newAccountSectionComponent}>
            <Text style={styles.newAccountExplanationText}>
              Don't have an account?
            </Text>
            <TouchableOpacity onPress={Actions.onNavigateToRegister}>
              <Text style={styles.newAccountNavigatorText}>Sign up here</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Login;
