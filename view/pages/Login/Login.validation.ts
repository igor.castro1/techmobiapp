import * as yup from "yup";

export const LoginFormSchema = yup.object({
  user: yup.string().required(),
  password: yup.string().required(),
});
