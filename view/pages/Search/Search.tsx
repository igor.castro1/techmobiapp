import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import BaseFooterView from "../../components/BaseFooterView/BaseFooterView";

const Search = ({ navigation }: CommonNavigationProps) => {
  return (
    <BaseFooterView navigation={navigation}>
      <Text>Search</Text>
    </BaseFooterView>
  );
};

export default Search

const styles = StyleSheet.create({})