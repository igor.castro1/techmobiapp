import { StyleSheet, Text, View } from "react-native";
import React from "react";
import BaseFooterView from "../../components/BaseFooterView/BaseFooterView";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import UseTicketsController from "./Tickets.controller";

const Tickets = ({ navigation }: CommonNavigationProps) => {
  const { Actions, States } = UseTicketsController({ navigation });
  return (
    <BaseFooterView navigation={navigation}>
      <Text>Tickets</Text>
    </BaseFooterView>
  );
};

export default Tickets;
