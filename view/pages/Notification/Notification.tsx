import { Text, TouchableOpacity, View } from "react-native";
import React from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import BaseFooterView from "../../components/BaseFooterView/BaseFooterView";
import UseNotificationController from "./Notification.controller";
import { TicketCard } from "../../components/TicketsCard/TicketCards";
import { EVehicleType } from "../../../business/domain/enums/EVehicleType";
import TruckImage from "../../../assets/truck.jpg";
import { styles } from "./Notification.styles";
import { ScrollView } from "react-native-gesture-handler";
import { MaterialIcons } from "@expo/vector-icons";

const Notification = ({ navigation }: CommonNavigationProps) => {
  const { Actions, States } = UseNotificationController({ navigation });

  return (
    <BaseFooterView navigation={navigation}>
      <View style={styles.baseComponent}>
        <View style={styles.backNavigationContainer}>
          <TouchableOpacity
            onPress={Actions.onGoBack}
            style={styles.opacityStyle}
          >
            <MaterialIcons
              style={styles.iconStyle}
              name="arrow-back-ios"
              size={20}
              color="orange"
            />
          </TouchableOpacity>
          <Text style={styles.notificationText}>Notifications</Text>
        </View>
        <View style={styles.ticketsComponent}>
          {[1, 2, 3, 4, 5].length >= 5 ? (
            <ScrollView>
              <View style={styles.ticketCardContainer}>
                <Text style={styles.ticketDayAmount}>Today</Text>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <Text style={styles.ticketDayAmount}>This Month</Text>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>

                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                   dates={{
                      createdAt: new Date(),
                      timeToNow: true,
                      invert: true,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
              </View>
            </ScrollView>
          ) : (
            <View style={styles.ticketCardContainer}>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  dates={{
                    createdAt: new Date(),
                    timeToNow: false,
                    invert: true,
                  }}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
            </View>
          )}
        </View>
      </View>
    </BaseFooterView>
  );
};

export default Notification;
