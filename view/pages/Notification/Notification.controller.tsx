import { useCallback } from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

const UseNotificationController = ({ navigation }: CommonNavigationProps) => {
  const navigateForLastRoute = useCallback(() => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, []);

  return {
    Actions: {
      onGoBack: navigateForLastRoute,
    },
    States: {},
  };
};

export default UseNotificationController;
