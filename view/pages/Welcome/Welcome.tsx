import HumanSvg from "../../../assets/human.png";
import WhiteLogo from "../../../assets/techmobi_white_logo.png";
import React from "react";
import UseWelcomeController from "./Welcome.controller";
import CustomButton from "../../components/CustomButton/CustomButton";
import { Image, Text, View } from "react-native";
import { styles } from "./Welcome.styles";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

const Welcome = ({ navigation }: CommonNavigationProps) => {
  const { Actions } = UseWelcomeController({ navigation });
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Image style={styles.logo} source={WhiteLogo} />
        <Text style={styles.welcomeText}>Welcome To techmobi</Text>
        <Text style={styles.presentationText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod
        </Text>
        <View style={styles.buttonContainer}>
          <CustomButton
            onPress={() => Actions.onNavigate()}
            title="Continue"
            variant={"transparent"}
          />
        </View>
      </View>
      <Image style={styles.human} source={HumanSvg} />
    </View>
  );
};

export default Welcome;
