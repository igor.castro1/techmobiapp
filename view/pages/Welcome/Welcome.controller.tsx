import { useCallback } from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

const UseWelcomeController = ({ navigation }: CommonNavigationProps) => {
  const navigateToLogin = useCallback(() => {
    navigation.navigate("Login");
  }, []);

  return {
    Actions: {
      onNavigate: navigateToLogin,
    },
  };
};

export default UseWelcomeController;
