import { useMemo } from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import UseFormTouch from "../../extensions/UseFormTouch";
import { useForm, SubmitHandler } from "react-hook-form";

type HomeFormType = {
  location: string;
  search: string;
};

const UseHomeController = ({ navigation }: CommonNavigationProps) => {
  const { touch, touched } = UseFormTouch();
  const { handleSubmit, control } = useForm<HomeFormType>();

  const locationItems = useMemo((): Array<{
    label: string;
    value: unknown;
  }> => {
    return [
      {
        label: "Halal Lab office",
        value: 1,
      },
      {
        label: "Halal Lab work",
        value: 2,
      },
      {
        label: "Halal Lab",
        value: 3,
      },
      {
        label: "home",
        value: 4,
      },
    ];
  }, []);

  return {
    Actions: {},
    States: {
      navigation,
      locationItems,
    },
    Helpers: {
      control,
    },
  };
};

export default UseHomeController;
