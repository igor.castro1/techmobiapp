import { View, Text, Image } from "react-native";
import React from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import BaseFooterView from "../../components/BaseFooterView/BaseFooterView";
import UseHomeController from "./Home.controller";
import { styles } from "./Home.styles";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import {
  DashboardDefaultIcon,
  FilterDefaultIcon,
  SearchIcon,
} from "../../components/Icons/Icons";
import { TicketCard } from "../../components/TicketsCard/TicketCards";
import TruckImage from "../../../assets/truck.jpg";
import ManImage from "../../../assets/man.jpg";
import { EVehicleType } from "../../../business/domain/enums/EVehicleType";
import Select from "../../components/Input/Select/Select";

const Home = ({ navigation }: CommonNavigationProps) => {
  const { Actions, States, Helpers } = UseHomeController({ navigation });

  return (
    <BaseFooterView navigation={States.navigation}>
      <View style={styles.baseComponent}>
        <View style={styles.circleComponentView}>
          <View style={styles.higherCircle}></View>
          <View style={styles.mediumCircle}></View>
          <View style={styles.smallerCircle}></View>
        </View>
        <View style={styles.homeViewMainInformation}>
          <View style={styles.homeLocationContainer}>
            <View style={styles.locationImageContainer}>
              <Image source={ManImage} style={styles.locationImage} />
              <View style={styles.locationSelectContainer}>
                <Text style={styles.locationSelectTitle}>LOCATION</Text>
                <Select
                  control={Helpers.control}
                  name="location"
                  items={States.locationItems}
                />
              </View>
            </View>
            <View style={styles.locationSearchContainer}>
              <View style={styles.locationSearchIcon}>
                <TouchableOpacity style={styles.locationSearchOpacity}>
                  <SearchIcon color="white" size={20} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.welcomeContainer}>
            <Text style={styles.welcomeText}>Hey Soban, Good Afternoon!</Text>
          </View>
          <View style={styles.techmobiContainer}>
            <Text style={styles.techmobiText}>Streamline your</Text>
            <Text style={styles.techmobiText}>Operations with TechMobi</Text>
          </View>
        </View>
        <View style={styles.ticketsComponent}>
          <View style={styles.ticketTitleContainer}>
            <Text style={styles.ticketTitle}>Tickets</Text>
            <View style={styles.ticketsTitleIconsContainer}>
              <TouchableOpacity>
                <FilterDefaultIcon color="black" size={24} />
              </TouchableOpacity>
              <TouchableOpacity>
                <DashboardDefaultIcon color="black" size={24} />
              </TouchableOpacity>
            </View>
          </View>
          {[1, 2, 3, 4, 5].length >= 5 ? (
            <ScrollView>
              <View style={styles.ticketCardContainer}>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
                <TicketCard.Container onPress={() => {}} haveBorder>
                  <TicketCard.TicketImage source={TruckImage} />
                  <TicketCard.Content
                    ticketNumber={"Ticket#2348"}
                    ticketStatus="Overweight Detected for the ticket"
                    dates={{
                      createdAt: new Date(),
                      timeToNow: false,
                      invert: false,
                    }}
                    vehicle={{
                      type: EVehicleType.Truck,
                      vehiclePlate: "IJN 8947",
                    }}
                  />
                </TicketCard.Container>
              </View>
            </ScrollView>
          ) : (
            <View style={styles.ticketCardContainer}>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  dates={{
                    createdAt: new Date(),
                    timeToNow: false,
                    invert: false,
                  }}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
            </View>
          )}
        </View>
      </View>
    </BaseFooterView>
  );
};

export default Home;
