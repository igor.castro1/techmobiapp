import { Text, TouchableOpacity, View, Image } from "react-native";
import React from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";
import BaseFooterView from "../../components/BaseFooterView/BaseFooterView";
import PersonImage from "../../../assets/man_with_background.jpg";
import { styles } from "./User.styles";
import { MaterialIcons } from "@expo/vector-icons";
import UseUserController from "./User.controller";
import {
  EarPhonesDefaultIcon,
  InfoDefaultIcon,
  LanguageDefaultIcon,
  LockDefaultIcon,
  LogoutDefaultIcon,
  PlusDefaultIcon,
  UserDefaultIcon,
} from "../../components/Icons/Icons";
import { Colors } from "../../static/Colors";

const User = ({ navigation }: CommonNavigationProps) => {
  const { Actions } = UseUserController({ navigation });

  return (
    <BaseFooterView navigation={navigation}>
      <View style={styles.baseComponent}>
        <View style={styles.backNavigationContainer}>
          <TouchableOpacity
            onPress={Actions.onGoBack}
            style={styles.opacityStyle}
          >
            <MaterialIcons
              style={styles.iconStyle}
              name="arrow-back-ios"
              size={20}
              color="orange"
            />
          </TouchableOpacity>
          <Text style={styles.notificationText}>Profile</Text>
        </View>
        <View style={styles.profileComponent}>
          <View style={styles.personalInformationComponent}>
            <View style={styles.personalInformationTextsComponent}>
              <Text style={styles.emailText}>Sobansaeed.870@gmail.com</Text>
              <Text style={styles.nameText}>Soban Saeed</Text>
              <Text style={styles.statusText}>Driver</Text>
            </View>
            <View style={styles.personalInformationProfilePickComponent}>
              <View style={styles.imageContainer}>
                <Image style={styles.image} source={PersonImage} />
              </View>
              <TouchableOpacity style={styles.iconOpacity}>
                <PlusDefaultIcon color="white" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.aboutMeContainer}>
            <Text style={styles.aboutMeTitle}>About me</Text>
            <Text style={styles.aboutMe}>
              Illum sadipscing sit et. Sed nonumy vel sea sed duo diam takimata
              lorem clita consequat laoreet accumsan at sadipscing takimata.
            </Text>
          </View>
          <View style={styles.settingsContainer}>
            <Text style={styles.settingsTitle}>Settings</Text>
            <TouchableOpacity style={styles.touchableSettings}>
              <UserDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Account</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <LockDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Privacy & Security</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <EarPhonesDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Help and support</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <InfoDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>About</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <LanguageDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Language</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <LogoutDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </BaseFooterView>
  );
};

export default User;
