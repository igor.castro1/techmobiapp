import { useCallback } from "react";
import { CommonNavigationProps } from "../../extensions/Navigation.type";

const UseUserController = ({ navigation }: CommonNavigationProps) => {
  const navigateForLastRoute = useCallback(() => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, []);

  return {
    Actions: {
      onGoBack: navigateForLastRoute,
    },
  };
};

export default UseUserController;
